import { GC_USER_ID, GC_AUTH_TOKEN } from './constants';

export function getUserId() {
  if (typeof localStorage !== 'undefined') {
    return localStorage.getItem(GC_USER_ID);
  }
  return false;
}

export function logout() {
  if (typeof localStorage !== 'undefined') {
    localStorage.removeItem(GC_USER_ID);
    localStorage.removeItem(GC_AUTH_TOKEN);
  }
  this.props.history.push('/');
}
