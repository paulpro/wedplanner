import React, { Fragment } from 'react';
import { string, bool, func } from 'prop-types';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import { ERROR_COLOR } from './../../constants';
import Button from '../styled/button';
import BordlessInput from '../styled/bordlessInput';
import FixedLabel from '../styled/fixedLabel';

const props = {
  shouldLogin: bool,
  revertState: func.isRequired,
  handleInputChange: func.isRequired,
  handleFormSubmit: func.isRequired,
  error: string.isRequired,
  embedded: bool,
};

function LoginComponent({
  handleInputChange,
  handleFormSubmit,
  shouldLogin,
  revertState,
  error,
  embedded,
}) {
  return (
    <Main>
      {error && <Error>{error}</Error>}
      {shouldLogin ? (
        <Fragment>
          <Helmet>
            <title>Вход</title>
            <meta name="description" content="Войдите в свою учетную запись." />
          </Helmet>
          <Title>Чтобы войти введите свою почту и пароль:</Title>
        </Fragment>
      ) : (
        <Fragment>
          <Helmet>
            <title>Регистрация</title>
            <meta name="description" content="Создайте учетную запись и начните пользоваться приложением." />
          </Helmet>
          <Title>Для регистрации введите имя, почту и пароль:</Title>
        </Fragment>
      )}
      <form>
        {!shouldLogin && (
          <div>
            <FixedLabel>Ваше имя</FixedLabel>
            <BordlessInput name="name" required onChange={handleInputChange} />
          </div>
        )}
        <div>
          <FixedLabel>Email</FixedLabel>
          <BordlessInput type="email" name="email" required onChange={handleInputChange} />
        </div>
        <div>
          <FixedLabel>Пароль</FixedLabel>
          <BordlessInput type="password" name="password" required onChange={handleInputChange} />
        </div>
        <SubmitButton onClick={handleFormSubmit}>{shouldLogin ? 'Войти' : 'Создать'}</SubmitButton>
        {!embedded && (
          <span>
            {' / '}
            <SwapMode onClick={revertState}>
              {shouldLogin ? 'зарегистрироваться' : 'войти'}
            </SwapMode>
          </span>
        )}
      </form>
    </Main>
  );
}

const Main = styled.main`
  margin-right: 1em;
  margin-left: 1em;
`;
const Title = styled.h3`
  font-weight: 400;
`;
const Error = styled.div`
  color: white;
  background-color: ${ERROR_COLOR};
  box-shadow: 0px 0px 0px 2px rgba(0, 0, 0, 0.6) inset;
  padding: calc(0.3em + 2px);
  display: flex;
  justify-content: center;
`;
const SubmitButton = styled(Button)`
  margin-left: 0;
`;
const SwapMode = styled.span`
  cursor: pointer;
  border-bottom: 1px dashed;
`;

LoginComponent.propTypes = props;
LoginComponent.defaultProps = { shouldLogin: true, embedded: false };

export default LoginComponent;
