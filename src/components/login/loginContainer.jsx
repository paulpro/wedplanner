import React from 'react';
import { gql, graphql } from 'react-apollo';
import { compose } from 'recompose';

import LoginComponent from './loginComponent';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../../constants';

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: props.defaultMode ? props.defaultMode : 'login',
      errorMessage: '',
    };
  }

  render() {
    return (
      <LoginComponent
        handleInputChange={this.onInput}
        handleFormSubmit={this.onSubmit}
        shouldLogin={this.state.mode === 'login'}
        revertState={this.revertMode}
        error={this.state.errorMessage}
        embedded={this.props.embedded}
      />
    );
  }

  onInput = e => this.setState({ [e.target.name]: e.target.value });
  revertMode = () =>
    this.setState(prevState => {
      return { mode: prevState.mode === 'login' ? 'register' : 'login' };
    });
  onSubmit = async e => {
    e.preventDefault();
    const { name, email, password } = this.state;
    if (!email) {
      this.setState({ errorMessage: 'Введите вашу почту!' });
      return;
    }
    if (!password) {
      this.setState({ errorMessage: 'Введите ваш пароль!' });
      return;
    }

    // REGISTRATION
    if (this.state.mode !== 'login') {
      if (!name) {
        this.setState({ errorMessage: 'Введите ваше имя!' });
        return;
      }

      await this.props.createUserMutation({
        variables: {
          name,
          email,
          password,
        },
      })
      .then(this.setState({ errorMessage: '' }))
      .catch(e => {
        if (e.message === 'GraphQL error: User already exists with that information') {
          this.setState({ errorMessage: 'Эта почта уже используется!' });
        } else {
          console.log(e);
        }
        return;
      });
    }

    // LOGIN
    if (!this.state.errorMessage || this.state.mode === 'login') {
      await this.props.signinUserMutation({
        variables: {
          email,
          password,
        },
      })
      .then(result => {
        const id = result.data.signinUser.user.id;
        const token = result.data.signinUser.token;
        this.saveUserData(id, token);
        this.props.history.push('/');
      })
      .catch(e => {
        if (e.message === 'GraphQL error: No user found with that information') {
          this.setState({ errorMessage: 'Неправильный пароль!' });
        } else {
          console.log(e);
        }
      });
    }
  };

  saveUserData = (id, token) => {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
  };
}

const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation($name: String!, $email: String!, $password: String!) {
    createUser(name: $name, authProvider: { email: { email: $email, password: $password } }) {
      id
    }
  }
`;

const SIGNIN_USER_MUTATION = gql`
  mutation SigninUserMutation($email: String!, $password: String!) {
    signinUser(email: { email: $email, password: $password }) {
      token
      user {
        id
      }
    }
  }
`;

export default compose(
  graphql(CREATE_USER_MUTATION, { name: 'createUserMutation' }),
  graphql(SIGNIN_USER_MUTATION, { name: 'signinUserMutation' }),
)(LoginContainer);
