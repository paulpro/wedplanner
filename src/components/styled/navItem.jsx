import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import { DARK_COLOR } from './../../constants';

const NavCSS = css`
  color: ${DARK_COLOR};
  border-radius: 3px;
  display: inline-block;
  font-weight: 400;
  letter-spacing: 0.06em;
  text-decoration: none;
  padding: ${props => (props.outlined ? '5px 10px' : '10px 15px')};
  margin: ${props => (props.outlined ? '5px 5px' : '')};
  min-width: 77px;
  text-align: center;
  border: 1px ${props => (props.outlined ? 'solid' : 'none')};

  &:hover {
    background: rgba(20, 20, 20, 0.1);
  }
`;

const NavItem = styled(Link)`
  ${NavCSS};
`;

export const HashNavItem = styled(HashLink)`
  ${NavCSS};
`;

export default NavItem;
