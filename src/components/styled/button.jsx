import styled from 'styled-components';
import { DARK_COLOR } from './../../constants';

const Button = styled.button`
  background: none;
  color: ${DARK_COLOR};
  cursor: pointer;
  font-family: 'Fira Sans';
  font-weight: 400;
  font-size: 1em;
  border: solid 1px ${DARK_COLOR};
  border-radius: 5px;
  margin: 5px;
  padding: 5px 10px;

  &:focus {
    outline: none;
  }
  &:hover {
    filter: brightness(0.85);
    border: solid 1px black;
  }
  &:active {
    filter: brightness(0.7);
    border: solid 1px black;
  }
`;

export default Button;
