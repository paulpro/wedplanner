import styled from 'styled-components';
import { DARK_COLOR, ACCENT_COLOR } from './../../constants';

const BordlessInput = styled.input`
  background: none;
  font-size: 0.9em;
  border: none;
  border-bottom: solid 2px ${DARK_COLOR};
  transition: border 0.5s;
  appearance: none;
  box-shadow: none;
  border-radius: none;
  padding: 5px;
  margin: 10px 10px 10px 0px;

  &:focus {
    border-bottom: solid 2px ${ACCENT_COLOR};
    outline: none;
  }
  &:hover {
    border-bottom: solid 2px black;
  }
  &:hover:focus {
    border-bottom: solid 2px ${ACCENT_COLOR};
  }

  @media screen and (max-width: 500px) {
    width: 48%;
  }
`;

export default BordlessInput;
