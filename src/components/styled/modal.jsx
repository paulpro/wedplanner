import React, { Fragment } from 'react';
import { node, bool, func } from 'prop-types';
import { withState } from 'recompose';
import styled from 'styled-components';
import { LIGHT_COLOR } from './../../constants';

import Button from './button';

const props = {
  children: node.isRequired,
  actionElement: func.isRequired,
  isOpen: bool.isRequired,
  setIsOpen: func.isRequired,
  okHandler: func.isRequired,
};

function Modal({
  children, actionElement, isOpen, setIsOpen, okHandler,
}) {
  const swapVisibility = () => setIsOpen(n => !n);
  return (
    <Fragment>
      {actionElement(swapVisibility)}
      {isOpen && (
        <FullScreen>
          <ModalWrapper>
            {children}
            <Div float="right">
              <Button onClick={swapVisibility}>Отмена</Button>
              <Button onClick={okHandler}>ОК</Button>
            </Div>
          </ModalWrapper>
        </FullScreen>
      )}
    </Fragment>
  );
}

Modal.propTypes = props;

const FullScreen = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  background-color: rgba(0, 0, 0, 0.6);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ModalWrapper = styled.div`
  background-color: ${LIGHT_COLOR};
  padding: 1.5em;
`;

const Div = styled.div`
  ${p => p};
`;

export default withState('isOpen', 'setIsOpen', false)(Modal);
