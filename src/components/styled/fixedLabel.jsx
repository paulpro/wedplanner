import styled from 'styled-components';

const FixedLabel = styled.label`
  display: inline-block;
  min-width: 6em;
`;

export default FixedLabel;
