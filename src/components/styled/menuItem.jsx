import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { DARK_COLOR, ACCENT_COLOR } from './../../constants';

const MenuItem = styled(NavLink)`
  color: ${DARK_COLOR};
  font-size: 2em;
  font-weight: 800;
  margin: 0 1em 2px;
  display: inline-block;
  line-height: 0.85;
  text-decoration: none;
  transition: transform 250ms, color 0.5s ease-in;
  @media screen and (max-width: 424px) {
    margin: 0 0.5em 2px;
    font-size: 1.7em;
  }

  &:hover {
    cursor: pointer;
    transform: scale(1.1);
    color: black;
  }

  &.${props => props.activeClassName} {
    border-bottom: 2px dashed;
    cursor: default;
    transform: none;
    color: ${ACCENT_COLOR};
    margin-bottom: 0;
  }
`;

MenuItem.defaultProps = {
  activeClassName: 'selected',
};

export default MenuItem;
