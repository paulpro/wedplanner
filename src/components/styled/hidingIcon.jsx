import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

const HidingIcon = styled(FontAwesomeIcon)`
  visibility: hidden;
  margin: 0px 5px;
  cursor: pointer;
  div:hover > & {
    visibility: visible;
  }
`;

export default HidingIcon;
