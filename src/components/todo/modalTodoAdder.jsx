import React from 'react';
import { string, bool, number, func, oneOfType } from 'prop-types';
import { graphql, gql } from 'react-apollo';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus';

import { GC_USER_ID } from '../../constants';
import ModalTodoForm from './modalTodoForm';
import Button from '../styled/button';

const props = {
  createTaskMutation: func.isRequired,
  value: string,
  isTodo: bool,
  date: oneOfType([number, string]),
  isCalendar: bool,
  budget: number,
  isBudget: bool,
};

const defaults = {
  value: '',
  isTodo: false,
  date: null,
  isCalendar: false,
  budget: 0,
  isBudget: false,
};

function ModalTodoAdder({
  createTaskMutation, value, isTodo, date, isCalendar, budget, isBudget,
}) {
  const formDefaults = {
    value,
    isTodo: value ? true : isTodo,
    date,
    isCalendar: date ? true : isCalendar,
    budget,
    isBudget: budget ? true : isBudget,
  };
  const createTask = async (
    formValue,
    formIsTodo,
    formDate,
    formIsCalendar,
    formBudget,
    formIsBudget,
  ) => {
    const postedById = localStorage.getItem(GC_USER_ID);
    if (!postedById) {
      console.error('No user logged in');
      return;
    }
    await createTaskMutation({
      variables: {
        value: formValue,
        isTodo: formIsTodo,
        date: formDate,
        isCalendar: formIsCalendar,
        budget: formBudget,
        isBudget: formIsBudget,
        postedById,
      },
    });
    location.reload();
  };
  return (
    <ModalTodoForm
      requestHandler={createTask}
      placeholders={formDefaults}
      modalTitle="Создать задачу"
      actionElement={clickHandler => (
        <Button onClick={clickHandler}>
          <FontAwesomeIcon icon={faPlus} /> Добавить задачу
        </Button>
      )}
    />
  );
}

ModalTodoAdder.propTypes = props;
ModalTodoAdder.defaultProps = defaults;

const CREATE_TASK_MUTATION = gql`
  mutation CreateTaskMutation(
    $value: String!
    $isTodo: Boolean!
    $date: DateTime
    $isCalendar: Boolean!
    $budget: Int!
    $isBudget: Boolean!
    $postedById: ID!
  ) {
    createTask(
      value: $value
      isTodo: $isTodo
      date: $date
      isCalendar: $isCalendar
      budget: $budget
      isBudget: $isBudget
      postedById: $postedById
    ) {
      id
      createdAt
      value
      isTodo
      date
      isCalendar
      budget
      isBudget
      postedBy {
        id
        name
      }
    }
  }
`;

export default graphql(CREATE_TASK_MUTATION, { name: 'createTaskMutation' })(ModalTodoAdder);
