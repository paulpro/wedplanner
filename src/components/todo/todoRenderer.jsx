import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { gql, withApollo } from 'react-apollo';
import faTrashAlt from '@fortawesome/fontawesome-free-solid/faTrashAlt';

import { MUTED_COLOR } from './../../constants';
import ModalTodoEditor from './modalTodoEditor';
import HidingIcon from '../styled/hidingIcon';

const props = {
  todos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })).isRequired,
  client: PropTypes.object.isRequired,
};

function TodoRenderer({ todos, client }) {
  async function editTask(updatedTask) {
    await client.mutate({
      mutation: UPDATE_TASK_MUTATION,
      variables: { id: updatedTask.id, done: updatedTask.done, visible: updatedTask.visible },
    });
    location.reload();
  }
  return (
    <Fragment>
      {todos.map((todo) => {
        const checkHandler = () => editTask({ ...todo, done: !todo.done });
        const trashHandler = () => editTask({ ...todo, visible: false });
        return (
          <Div margin="1em 0" key={todo.id}>
            <InlineTodo onClick={checkHandler} done={todo.done}>
              {todo.value}
            </InlineTodo>
            <ModalTodoEditor todo={todo} />
            <HidingIcon icon={faTrashAlt} onClick={trashHandler} />
          </Div>
        );
      })}
    </Fragment>
  );
}

TodoRenderer.propTypes = props;

const Div = styled.div`
  ${p => p};
`;
const DonedTaskStyle = css`
  color: ${MUTED_COLOR};
  text-decoration: line-through;
`;
const InlineTodo = styled.div`
  cursor: pointer;
  display: inline;
  ${props => (props.done ? DonedTaskStyle : '')} &:hover {
    ${DonedTaskStyle};
  }
  &::before {
    content: '× ';
  }
`;

const UPDATE_TASK_MUTATION = gql`
  mutation UpdateTaskMutation($id: ID!, $done: Boolean, $visible: Boolean) {
    updateTask(id: $id, done: $done, visible: $visible) {
      id
      done
      visible
      createdAt
      value
      isTodo
      date
      isCalendar
      budget
      isBudget
    }
  }
`;

export default withApollo(TodoRenderer);
