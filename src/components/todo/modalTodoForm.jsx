import React from 'react';
import { bool, func, string, number, oneOfType, shape } from 'prop-types';
import { compose, withState } from 'recompose';
import styled from 'styled-components';

import Modal from '../styled/modal';
import BordlessInput from '../styled/bordlessInput';

const props = {
  requestHandler: func.isRequired,
  placeholders: shape({
    value: string,
    isTodo: bool,
    date: oneOfType([number, string]),
    isCalendar: bool,
    budget: number,
    isBudget: bool,
  }).isRequired,
  modalTitle: string.isRequired,
  actionElement: func.isRequired,
  value: string.isRequired,
  setValue: func.isRequired,
  isTodo: bool.isRequired,
  setIsTodo: func.isRequired,
  date: oneOfType([number, string]),
  setDate: func.isRequired,
  isCalendar: bool.isRequired,
  setIsCalendar: func.isRequired,
  budget: number.isRequired,
  setBudget: func.isRequired,
  isBudget: bool.isRequired,
  setIsBudget: func.isRequired,
};

function ModalTodoForm({
  requestHandler,
  modalTitle,
  actionElement,
  value,
  setValue,
  isTodo,
  setIsTodo,
  date,
  setDate,
  isCalendar,
  setIsCalendar,
  budget,
  setBudget,
  isBudget,
  setIsBudget,
}) {
  const swapIsTodo = () => setIsTodo(n => !n);
  const swapIsCalendar = () => setIsCalendar(n => !n);
  const swapIsBudget = () => setIsBudget(n => !n);
  return (
    <Modal
      okHandler={() => requestHandler(value, isTodo, date, isCalendar, budget, isBudget)}
      actionElement={actionElement}
    >
      <Title>{modalTitle}</Title>
      <form>
        <div>
          <label>
            Добавить в список дел<input
              type="checkbox"
              name="isTodo"
              checked={isTodo}
              onChange={swapIsTodo}
            />
          </label>
        </div>
        {isTodo && (
          <div>
            <FieldLabel>Текст задачи</FieldLabel>
            <BordlessInput name="value" value={value} onChange={e => setValue(e.target.value)} />
          </div>
        )}
        <div>
          <label>
            Добавить в бюджет<input
              type="checkbox"
              name="isBudget"
              checked={isBudget}
              onChange={swapIsBudget}
            />
          </label>
        </div>
        {isBudget && (
          <div>
            <FieldLabel>Бюджет задачи</FieldLabel>
            <BordlessInput
              name="budget"
              type="number"
              step="1000"
              value={budget}
              onChange={e => setBudget(parseInt(e.target.value))}
            />
          </div>
        )}
        {/* <div>
          <label>
            Добавить в календарь<input
              type="checkbox"
              name="isCalendar"
              checked={isCalendar}
              onChange={swapIsCalendar}
            />
          </label>
        </div>
        {isCalendar && (
          <div>
            <FixedLabel>Дата выполнения</FixedLabel>
            <BordlessInput
              name="date"
              type="date"
              value={date === null ? undefined : date} // BUG, see https://fb.me/react-controlled-components
              onChange={e => setDate(e.target.value)}
            />
          </div>
        )} */}
      </form>
    </Modal>
  );
}

ModalTodoForm.propTypes = props;

const FieldLabel = styled.label`
  display: inline-block;
  min-width: 8em;
`;
const Title = styled.h2`
  text-align: center;
`;

const enchance = compose(
  withState('value', 'setValue', props => props.placeholders.value),
  withState('isTodo', 'setIsTodo', props => props.placeholders.isTodo),
  withState('date', 'setDate', props => props.placeholders.date),
  withState('isCalendar', 'setIsCalendar', props => props.placeholders.isCalendar),
  withState('budget', 'setBudget', props => props.placeholders.budget),
  withState('isBudget', 'setIsBudget', props => props.placeholders.isBudget),
);

export default enchance(ModalTodoForm);
