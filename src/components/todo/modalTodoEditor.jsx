import React from 'react';
import { string, bool, number, func, oneOfType, shape } from 'prop-types';
import { graphql, gql } from 'react-apollo';
import faPencilAlt from '@fortawesome/fontawesome-free-solid/faPencilAlt';
import HidingIcon from '../styled/hidingIcon';

import { GC_USER_ID } from '../../constants';
import ModalTodoForm from './modalTodoForm';

const props = {
  updateTaskMutation: func.isRequired,
  todo: shape({
    id: string,
    value: string,
    isTodo: bool,
    date: oneOfType([number, string]),
    isCalendar: bool,
    budget: number,
    isBudget: bool,
  }).isRequired,
};

function ModalTodoEditor({ updateTaskMutation, todo }) {
  const formDefaults = {
    value: '',
    isTodo: false,
    date: null,
    isCalendar: false,
    budget: 0,
    isBudget: false,
    ...todo,
  };
  const editTask = async (
    formValue,
    formIsTodo,
    formDate,
    formIsCalendar,
    formBudget,
    formIsBudget,
  ) => {
    const postedById = localStorage.getItem(GC_USER_ID);
    if (!postedById) {
      console.error('No user logged in');
      return;
    }
    await updateTaskMutation({
      variables: {
        taskId: todo.id,
        value: formValue,
        isTodo: formIsTodo,
        date: formDate,
        isCalendar: formIsCalendar,
        budget: formBudget,
        isBudget: formIsBudget,
      },
    });
    location.reload();
  };
  return (
    <ModalTodoForm
      requestHandler={editTask}
      placeholders={formDefaults}
      modalTitle="Изменить задачу"
      actionElement={clickHandler => <HidingIcon icon={faPencilAlt} onClick={clickHandler} />}
    />
  );
}

ModalTodoEditor.propTypes = props;

const UPDATE_TASK_MUTATION = gql`
  mutation UpdateTaskMutation(
    $taskId: ID!
    $value: String!
    $isTodo: Boolean!
    $date: DateTime!
    $isCalendar: Boolean!
    $budget: Int!
    $isBudget: Boolean!
  ) {
    updateTask(
      id: $taskId
      value: $value
      isTodo: $isTodo
      date: $date
      isCalendar: $isCalendar
      budget: $budget
      isBudget: $isBudget
    ) {
      id
      done
      visible
      createdAt
      value
      isTodo
      date
      isCalendar
      budget
      isBudget
    }
  }
`;

export default graphql(UPDATE_TASK_MUTATION, { name: 'updateTaskMutation' })(ModalTodoEditor);
