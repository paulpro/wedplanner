import React from 'react';
import PropTypes from 'prop-types';
import { gql, withApollo } from 'react-apollo';
import styled from 'styled-components';
import { compose, lifecycle, withState } from 'recompose';
import { Helmet } from 'react-helmet';

import { GC_USER_ID } from '../../constants';
import ModalTodoAdder from './modalTodoAdder';
import TodoRenderer from './todoRenderer';

const props = {
  todos: PropTypes.array.isRequired,
};

function Todo({ todos }) {
  if (todos === []) {
    return <div>Loading...</div>;
  }
  return (
    <StyledMain>
      <Helmet>
        <title>Список дел</title>
        <meta
          name="description"
          content="Ведите список свадебных дел, отмечайте выполненные и добавляйте их в бюджет."
        />
      </Helmet>
      <TodoRenderer todos={todos} />
      <ModalTodoAdder isTodo />
    </StyledMain>
  );
}

Todo.propTypes = props;

const StyledMain = styled.main`
  @media screen and (max-width: 424px) {
    margin: 0 0.5em;
  }
`;

async function loadTodos(client, setTodos) {
  const userId = localStorage.getItem(GC_USER_ID);
  if (userId === null) {
    setTodos([
      {
        id: 'ex1',
        value: 'Зайдите на сайт wedlist.org',
        done: true,
      },
      {
        id: 'ex2',
        value: 'Создайте аккаунт',
        done: false,
      },
      {
        id: 'ex3',
        value: 'Начинайте добавлять свадебные дела!',
        done: false,
      },
    ]);
    return;
  }
  const result = await client.query({
    query: USER_TASKS_QUERY,
    variables: { userId },
  });
  setTodos(result.data.allTasks);
}

const USER_TASKS_QUERY = gql`
  query UsersTasksQuery($userId: ID!) {
    allTasks(filter: { AND: [{ postedBy: { id: $userId } }, { visible: true }] }) {
      id
      done
      visible
      createdAt
      value
      isTodo
      date
      isCalendar
      budget
      isBudget
    }
  }
`;

const enchance = compose(
  withApollo,
  withState('todos', 'setTodos', []),
  lifecycle({
    componentDidMount() {
      loadTodos(this.props.client, this.props.setTodos);
    },
  }),
);

export default enchance(Todo);
