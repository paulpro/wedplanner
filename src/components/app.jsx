import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import LoginContainer from './login/loginContainer';
import Landing from './layout/landing';
import Todo from './todo/todo';
import Budget from './budget/budget';
import Layout from './layout/layout';
import Page from './layout/page';
import Header from './layout/header';
import Footer from './layout/footer';
import Handler404 from './layout/handler404';

const App = () => (
  <Layout>
    <Helmet
      titleTemplate="%s | Wedlist"
      defaultTitle="Wedlist — запланируй бюджет свадьбы и следи за его выполнением"
    />
    <Page>
      <Header />
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route path="/todo" component={Todo} />
        <Route path="/login" component={LoginContainer} />
        <Route path="/register" render={() => <LoginContainer defaultMode="register" />} />
        <Route path="/budget" component={Budget} />
        <Route component={Handler404} />
      </Switch>
      <Footer />
    </Page>
  </Layout>
);

export default App;
