import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { gql, withApollo } from 'react-apollo';
import { compose, lifecycle, withState, mapProps } from 'recompose';
import { Helmet } from 'react-helmet';

import { GC_USER_ID } from '../../constants';
import BudgetRows from './budgetRows';

const props = {
  todos: PropTypes.array.isRequired,
};

function Budget({ todos }) {
  return (
    <StyledMain>
      <Helmet>
        <title>Бюджет</title>
        <meta
          name="description"
          content="Планируйте бюджет вашей свадьбы, добавляйте новые дела и следите за балансом."
        />
      </Helmet>
      <Column>
        <ColumnHeader>Запланировано</ColumnHeader>
        <BudgetRows data={todos.filter(row => row.done === false)} />
      </Column>
      <Column>
        <ColumnHeader>Потрачено</ColumnHeader>
        <BudgetRows data={todos.filter(row => row.done === true)} />
      </Column>
    </StyledMain>
  );
}

Budget.propTypes = props;

const Column = styled.section`
  display: inline-flex;
  flex-direction: column;
  align-items: stretch;
  width: 20em;
  padding: 0 1em;
  margin: 1em 0;
  @media screen and (max-width: 767px) {
    margin: 2em 0;
  }
  &:not(:last-child) {
    @media screen and (min-width: 768px) {
      border-right: 1px solid #555;
    }
  }
  & :last-child {
    margin-top: auto;
  }
`;

const ColumnHeader = styled.div`
  align-self: center;
  text-transform: uppercase;
  margin-bottom: 0.4em;
  font-weight: 800;
`;

const StyledMain = styled.main`
  display: flex;
  @media screen and (max-width: 767px) {
    flex-flow: column;
  }
`;

async function loadTodos(client, setTodos) {
  const userId = localStorage.getItem(GC_USER_ID);
  if (userId === null) {
    setTodos([
      {
        id: 'firstExample',
        value: 'Try to login and write you first task!',
        done: false,
        budget: 1000000,
      },
    ]);
    return;
  }
  const result = await client.query({
    query: USER_TASKS_QUERY,
    variables: { userId },
  });
  setTodos(result.data.allTasks);
}

const USER_TASKS_QUERY = gql`
  query UsersTasksQuery($userId: ID!) {
    allTasks(filter: { AND: [{ postedBy: { id: $userId } }, { visible: true }] }) {
      id
      done
      value
      budget
      isBudget
      isTodo
    }
  }
`;

const enchance = compose(
  withApollo,
  withState('todos', 'setTodos', []),
  lifecycle({
    componentDidMount() {
      loadTodos(this.props.client, this.props.setTodos);
    },
  }),
  mapProps(oldprops => ({ todos: oldprops.todos.filter(row => row.isBudget === true) })),
);

export default enchance(Budget);
