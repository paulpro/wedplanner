import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MUTED_COLOR, LIGHT_COLOR } from './../../constants';

const props = {
  data: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    budget: PropTypes.number.isRequired,
  })).isRequired,
};

function BudgetRows({ data }) {
  return (
    <Fragment>
      {data.map(row => (
        <Row key={row.id}>
          <span>{row.value}</span>
          <span>{row.budget}</span>
        </Row>
      ))}
      <Row>
        <span>Всего:</span>
        <span>{data.reduce((acc, row) => acc + row.budget, 0)}</span>
      </Row>
    </Fragment>
  );
}

BudgetRows.propTypes = props;

const Row = styled.p`
  display: flex;
  justify-content: space-between;
  padding: 0.4em 0.5em;
  margin: 0 0.5em;

  &:hover {
    background-color: rgba(0, 0, 0, 0.08);
    border-radius: 3px;
  }
  &:last-of-type {
    background: ${MUTED_COLOR};
    color: ${LIGHT_COLOR};
  }
`;

export default BudgetRows;
