import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { MemoryRouter } from 'react-router-dom';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import '../global-styles';

import App from './app';

// Create stylesheet manager for styled-components
const sheet = new ServerStyleSheet();

const PrerenderedApp = (
  <StyleSheetManager sheet={sheet.instance}>
    <MemoryRouter>
      <App />
    </MemoryRouter>
  </StyleSheetManager>
);

export const prerendered = (function () {
  return ReactDOMServer.renderToString(PrerenderedApp);
}());

export const styleTags = sheet.getStyleTags();
