import React, { Fragment } from 'react';
import { bool, func, node } from 'prop-types';
import styled from 'styled-components';
import { withState } from 'recompose';

const props = {
  active: bool.isRequired,
  setActive: func.isRequired,
  children: node.isRequired,
  submitHandler: func.isRequired,
};

function DateLine({ active, setActive, children, submitHandler }) {
  return (
    <Fragment>
      <Text onClick={() => setActive(v => !v)}>{children}</Text>
      <Input active={active} >
        <input type="date" onChange={async (e) => { await submitHandler(e.target.value); location.reload(); }} />
      </Input>
    </Fragment>
  );
}

DateLine.propTypes = props;

const Text = styled.span`
  display: inline-block;
  line-height: 0.9;
  cursor: pointer;
  border-bottom: 1px solid;
`;

const Input = styled.div`
  order: 100;
  width: 100%;
  display: ${props => props.active ? 'flex' : 'none'};
  justify-content: center;
`;

export default withState('active', 'setActive', false)(DateLine);
