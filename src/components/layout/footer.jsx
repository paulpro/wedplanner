import React from 'react';
import styled, { keyframes } from 'styled-components';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faCopyright from '@fortawesome/fontawesome-free-solid/faCopyright';
import { ACCENT_COLOR, LIGHT_COLOR } from './../../constants';

const deny = keyframes`
  from {
    transform: translate(0);
  }
  33% {
    transform: translate(.2em);
    color: #fdd;
  }
  66% {
    transform: translate(-.2em);
    color: #fdd;
  }
  to {
    transform: translate(0);
  }
`;

const Rotate = styled.div`
  &:hover {
    animation: ${deny} 250ms ease-in-out;
  }
`;

const Footer = () => (
  <StyledFooter>
    <div>
      <FontAwesomeIcon style={{ height: '1em', verticalAlign: '-0.125em' }} icon={faCopyright} />{' '}
      {new Date().getFullYear()}, Wedlist Team
    </div>
    <Rotate>mail@wedlist.org</Rotate>
  </StyledFooter>
);

const StyledFooter = styled.footer`
  background-color: ${ACCENT_COLOR};
  color: ${LIGHT_COLOR};
  padding: 2em;
  margin-top: 0.5em;
  display: flex;
  justify-content: space-between;
`;

export default Footer;
