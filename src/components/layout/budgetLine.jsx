import React, { Fragment } from 'react';
import { bool, func, number, node } from 'prop-types';
import styled from 'styled-components';
import { withState, compose } from 'recompose';

const props = {
  active: bool.isRequired,
  setActive: func.isRequired,
  value: number.isRequired,
  setValue: func.isRequired,
  children: node.isRequired,
  submitHandler: func.isRequired,
};

function BudgetLine({ active, setActive, value, setValue, children, submitHandler }) {
  return (
    <Fragment>
      <Text onClick={() => setActive(v => !v)}>{children}</Text>
      <Input active={active} >
        <input type="text" onChange={e => setValue(parseInt(e.target.value))} />
        <button onClick={async () => { await submitHandler(value); location.reload(); }}>Сохранить</button>
      </Input>
    </Fragment>
  );
}

BudgetLine.propTypes = props;

const Text = styled.span`
  display: inline-block;
  line-height: 0.9;
  cursor: pointer;
  border-bottom: 1px solid;
`;

const Input = styled.div`
  order: 100;
  width: 100%;
  display: ${props => props.active ? 'flex' : 'none'};
  justify-content: center;
`;

const enchance = compose(
  withState('value', 'setValue', 0),
  withState('active', 'setActive', false),
);
export default enchance(BudgetLine);
