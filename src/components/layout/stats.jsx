import React from 'react';
import { bool, shape, string, object } from 'prop-types';
import { graphql, gql, withApollo } from 'react-apollo';
import styled from 'styled-components';
import { compose } from 'recompose';
import distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import { MUTED_COLOR } from './../../constants';

import BudgetLine from './budgetLine';
import DateLine from './dateLine';

const ruLocale = require('date-fns/locale/ru');

const props = {
  client: object.isRequired,
  userStatsQuery: shape({
    loading: bool,
    error: string,
    User: object,
  }).isRequired,
  userId: string.isRequired,
};

function Stats({ client, userStatsQuery, userId }) {
  const sendBudget = budget =>
    client.mutate({
      mutation: USER_BUDGET_MUTATION,
      variables: { userId, budget },
    });
  const sendDate = date =>
    client.mutate({
      mutation: USER_DATE_MUTATION,
      variables: { userId, date },
    });

  if (userStatsQuery && userStatsQuery.loading) {
    return <div />;
  }

  // destructuring
  const { allTasks: tasks } = userStatsQuery;
  const { plannedBudget, weddingDate } = userStatsQuery.User;
  // counting
  const undonnedTodos = tasks.filter(row => row.done === false && row.isTodo === true);
  const paidTasks = tasks.filter(row => row.done === true && row.isBudget === true);
  const paidTotal = paidTasks.reduce((acc, row) => acc + row.budget, 0);
  // making text
  const budgetText = plannedBudget ? (
    `остаток бюджета: ${plannedBudget - paidTotal}`
  ) : (
    <BudgetLine submitHandler={budgetValue => sendBudget(budgetValue)}>введите бюджет</BudgetLine>
  );
  const dateText = weddingDate ? (
    `Свадьба через ${distanceInWordsStrict(weddingDate, new Date(), { locale: ruLocale })}`
  ) : (
    <DateLine submitHandler={dateValue => sendDate(dateValue)}>Введите дату</DateLine>
  );
  const undonnedText = `незавершенных задач: ${undonnedTodos.length}`;

  return (
    <Flex>
      <String>{dateText}</String>
      <String>{undonnedText}</String>
      <String>{budgetText}</String>
    </Flex>
  );
}

Stats.propTypes = props;

const Flex = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: baseline;
  align-content: center;
  justify-content: center;
  font-size: 0.9em;
`;

const String = styled.span`
  color: ${MUTED_COLOR};
  display: inline-block;
  &:not(:last-child)::after {
    content: ', ';
    white-space: pre;
  }
`;

const USER_STATS_QUERY = gql`
  query UserStatsQuery($userId: ID!) {
    User(id: $userId) {
      plannedBudget
      weddingDate
    }
    allTasks(filter: { AND: [{ postedBy: { id: $userId } }, { visible: true }] }) {
      id
      done
      budget
      isBudget
      isTodo
    }
  }
`;

const USER_BUDGET_MUTATION = gql`
  mutation UserBudgetMutation($userId: ID!, $budget: Int!) {
    updateUser(id: $userId, plannedBudget: $budget) {
      id
      plannedBudget
    }
  }
`;

const USER_DATE_MUTATION = gql`
  mutation UserDateMutation($userId: ID!, $date: DateTime!) {
    updateUser(id: $userId, weddingDate: $date) {
      id
      weddingDate
    }
  }
`;

const enchance = compose(withApollo, graphql(USER_STATS_QUERY, { name: 'userStatsQuery' }));

export default enchance(Stats);
