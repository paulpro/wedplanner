import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

const Handler404 = () => (
  <main>
    <Helmet>
      <title>404! Такой страницы не существует</title>
      <meta name="description" content="Вы попали на данную страницу по ошибке. На самом деле ее не существует." />
    </Helmet>
    <h2>Упс! 💥</h2>
    <p>Что-то пошло не так и вы оказались на несуществующей странице (404).</p>
    <p>
      Если вам кажется, что эта страница нужна — пишите на
      <MailLink href="mailto:error@wedlist.org">error@wedlist.org</MailLink>
    </p>
  </main>
);

const MailLink = styled.a`
  color: navy;
  text-decoration: underline;
  padding-left: 0.3em;
`;

export default Handler404;
