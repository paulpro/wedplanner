import React, { Fragment } from 'react';
import { withProps } from 'recompose';
import { func } from 'prop-types';
import styled from 'styled-components';

import NavItem, { HashNavItem } from '../styled/navItem';
import MenuItem from '../styled/menuItem';
import Stats from './stats';
import { ACCENT_COLOR } from '../../constants';
import { getUserId, logout } from '../../utils';

const props = {
  loadUserId: func.isRequired,
  logoutHandler: func.isRequired,
};

const Header = ({ loadUserId, logoutHandler }) => (
  <Fragment>
    <StyledHeader>
      <Logo>
        Wed<Heart>❤</Heart>list
      </Logo>
      {loadUserId() ? (
        <Fragment>
          <Stats userId={loadUserId()} />
          <NavItem onClick={logoutHandler} to="/" outlined="true">
            Выйти
          </NavItem>
        </Fragment>
      ) : (
        <StyledHashes>
          <HashNavItem to="/#about">О нас</HashNavItem>
          <HashNavItem to="/#features">Функции</HashNavItem>
          <HashNavItem to="/#next">Скоро</HashNavItem>
          <NavItem to="/login" outlined="true">
            Войти
          </NavItem>
        </StyledHashes>
      )}
    </StyledHeader>
    {loadUserId() && (
      <StyledMenu>
        <MenuItem to="/todo">Список дел</MenuItem>
        <MenuItem to="/budget">Бюджет</MenuItem>
      </StyledMenu>
    )}
  </Fragment>
);

Header.propTypes = props;

const StyledHeader = styled.header`
  margin: 2em 2em;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media screen and (max-width: 700px) {
    margin: 1em 2em;
    flex-direction: column;
  }
`;
const StyledMenu = styled.menu`
  display: flex;
  justify-content: center;
  margin-bottom: 1.2em;
  padding: 0.5em 0 0;
  @media screen and (max-width: 424px) {
    justify-content: space-evenly;
  }
`;
const StyledHashes = styled.div`
  display: inline-grid;
  grid-template-areas: 'hash hash hash hash';

  @media screen and (max-width: 500px) {
    grid-template-areas: 'hash hash' 'hash hash';
  }
`;
const Logo = styled.span`
  font-family: 'Rubik';
  font-size: 2em;
  margin-right: 1em;
  line-height: 0.8;

  @media screen and (max-width: 700px) {
    margin: 0;
  }
`;
const Heart = styled.span`
  color: ${ACCENT_COLOR};
`;

export default withProps({
  loadUserId: getUserId,
  logoutHandler: logout,
})(Header);
