import React, { Fragment } from 'react';
import { Redirect, Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { compose, withState } from 'recompose';
import { bool, func } from 'prop-types';

import LoginContainer from '../login/loginContainer';
import { ACCENT_COLOR } from '../../constants';
import { getUserId as isUser } from '../../utils';

const props = {
  isExample: bool.isRequired,
  setIsExample: func.isRequired,
  exampleTodo: bool.isRequired,
  setExampleTodo: func.isRequired,
};

function Landing({ isExample, setIsExample, exampleTodo, setExampleTodo }) {
  const swapButton = () => setIsExample(n => !n);
  const swapExample = () => setExampleTodo(n => !n);

  if (isUser()) {
    return <Redirect to="/todo" />;
  }

  return (
    <Content>
      <MainText>Добро пожаловать в свадебный планировщик</MainText>
      <ActionButtons>
        <Link to="#signup">
          <ActionButton primary={!isExample} left onClick={swapButton}>
            
          </ActionButton>
        </Link>
        <Link to="#example">
          <ActionButton primary={isExample} onClick={swapButton}>
            
          </ActionButton>
        </Link>
      </ActionButtons>
      {isExample ? (
        <InfoBlock white fixHeight>
          <ImgHolder>
            {exampleTodo ? <img src="exampleTodo.opt.png" /> : <Fragment><img src="exampleBudget_1.opt.png" /><img src="exampleBudget_2.opt.png" /></Fragment>}
          </ImgHolder>
          <Swapper>
            <SwapMode onClick={swapExample} active={exampleTodo}>Список дел</SwapMode>
            {' / '}
            <SwapMode onClick={swapExample} active={!exampleTodo}>Бюджет</SwapMode>
          </Swapper>
        </InfoBlock>
      ) : (
        <InfoBlock white fixHeight>
          <LoginContainer defaultMode="register" embedded />
        </InfoBlock>
      )}
      <InfoBlock>
        <Circle id="about">о Wedlist</Circle>
        <BlockText>
          Мы создали планировщик для тех, у кого скоро свадьба. Это удобный лаконичный органайзер,
          способный хранить в одном месте список важных дел, вести свадебный бюджет, сохранять
          номера телефонов и другую информацию о мероприятии.
        </BlockText>
      </InfoBlock>
      <InfoBlock white>
        <StyledOl>
          <li>создание и редактирование задач,</li>
          <li>
            добавление бюджета к свадебному делу и просмотр информации о нем на финансовой
            странице,
          </li>
          <li>отметка о выполнении дела и контроль остатка по бюджету.</li>
        </StyledOl>
        <Circle id="features">функции</Circle>
      </InfoBlock>
      <InfoBlock>
        <Circle id="next">скоро</Circle>
        <StyledOl start="4">
          <li>синхронизация задач с календарем и отправка напоминаний на электронную почту,</li>
          <li>ведение списка контактов и создание заметок к делам.</li>
        </StyledOl>
      </InfoBlock>
      <MailBlock>
        <p>Если хотите оставаться в курсе обновлений — подпишитесь на наши новости</p>
      </MailBlock>
      <div>
        <EmailInput type="text" size="25" />
        <EmailButton>OK</EmailButton>
      </div>
    </Content>
  );
}

Landing.propTypes = props;

const Content = styled.main`
  margin: 0 0 6em 0 !important;
  line-height: 1.5;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const MainText = styled.h1`
  text-align: center;
  margin-top: 2em;
  margin-right: .5em;
  margin-left: .5em;
  max-width: 18em;

  @media screen and (max-width: 700px) {
    margin-top: 1em;
    font-size: 1.5em;
  }
`;
const ActionButtons = styled.div`
  margin-top: 2em;
  margin-bottom: 1em;
`;
const ActionButton = styled.button`
  background-color: white;
  color: ${props => (props.primary ? 'white' : ACCENT_COLOR)};
  font-size: 1.1em;
  font-family: inherit;
  letter-spacing: 0.03em;
  margin: 0;
  padding: 0.5em 1em;
  height: 2.6em;
  border: 2px solid ${ACCENT_COLOR};
  border-radius: ${props => (props.left ? '8px 0 0 8px' : '0 8px 8px 0')};
  cursor: pointer;
  transition: all 0.3s cubic-bezier(0.42, 0, 0.58, 1);
  position: relative;
  z-index: 1;

  &:before {
    position: absolute;
    top: 0px;
    height: 100%;
    width: ${props => (props.primary ? '100%' : '0px')};
    z-index: -1;
    content: '';
    background: ${ACCENT_COLOR};
    transition: all 0.3s cubic-bezier(0.42, 0, 0.58, 1);
    
    ${(props) => {
      const val = props.primary ? '0%' : '0px';
      const attr = props.left ? 'right' : 'left';
      return `${attr}: ${val};`;
    }};
  }

  &:after {
    content: ${props => (props.left ? '\'Зарегистрироваться\'' : '\'Посмотреть пример\'')};

    @media screen and (max-width: 700px) {
      content: ${props => (props.left ? '\'Начать\'' : '\'Пример\'')};
    }
  }

  &:focus {
    outline: none;
  }
`;
const ImgHolder = styled.div`
  width:100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 767px) {
    flex-flow: column;
  }

  & img {
    padding: 0.5em;
    @media screen and (max-width: 767px) {
      padding: 2em 0.5em;
    }

    @media screen and (max-width: 500px) {
      padding: 2em 0em;
      width: inherit;
    }
  }

  & :not(:last-child) {
    @media screen and (min-width: 768px) {
      border-right: 1px solid #555;
    }
  }
`;
const Swapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;  
`;
const SwapMode = styled.span`
  cursor: pointer;
  width: 6em;
  padding 0 .5em;
  font-weight: ${props => (props.active && 'bold')};
  text-decoration: underline;

  &:first-of-type {
    text-align: right;
  }
  /* border-bottom: 1px ${props => (props.active ? 'solid' : 'dashed')}; */
`;
const InfoBlock = styled.div`
  background-color: ${props => (props.white ? 'white' : 'transparent')};
  width: calc(100% - 6em);
  @media screen and (max-width: 500px) {
    width: calc(100% - 3em);
    padding: 2em 1em;
  }
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-around;
  padding: 5em 2em;
  ${props => (props.fixHeight && MainBlockCSS)}
  ${props => (props.white && !props.fixHeight && ReverseCircleCSS)}
`;
const MainBlockCSS = css`
  height: 15em;
  @media screen and (max-width: 767px) {
    height: auto;
  }
  align-content: space-between;
  margin-top: -2.5em;
  padding: 3em 2em;
  /* border: 2px solid ${ACCENT_COLOR}; */
  border-radius: 8px;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
`;
const Circle = styled.h3`
  height: 7em;
  width: 7em;
  margin: 1em 0.5em;
  border-radius: 50%;
  background: linear-gradient(to bottom right, #9c7531, hsl(38, 100%, 73%));
  color: white;
  line-height: 7em;
  text-align: center;
  font-weight: 400;
  font-size: 1.4em;
  box-shadow: 0px 5px 15px 0px rgba(0,0,0,0.19);
  
  &:first-of-type {
    margin-top: 0;
  }
`;
const ReverseCircleCSS = css`
  flex-wrap: wrap-reverse;
`;
const BlockText = styled.p`
  margin-top: 0;
  max-width: 28em;
`;
const StyledOl = styled.ol`
  list-style-position: inside;
  margin: 0 0 1em 0;
  padding: 0;
  max-width: 28em;
`;
const MailBlock = styled.div`
  margin-top: -3em;
  text-align: center;

  @media screen and (max-width: 500px) {
    margin: -1em 1em 0 1em;
  }
`;
const EmailInput = styled.input`
  border: 2px solid ${ACCENT_COLOR};
  border-radius: 5px 0 0 5px;
  font-size: 1em;
  padding: 6px;
  appearance: none;
  box-shadow: none;
  &:focus {
    outline: none;
  }
  &:hover {
    filter: brightness(0.96);
  }
`;
const EmailButton = styled.button`
  font-size: 1em;
  padding: 7px;
  border: 1px solid ${ACCENT_COLOR};
  border-radius: 0 5px 5px 0;
  background-color: ${ACCENT_COLOR};
  color: white;
  cursor: pointer;
  &:focus {
    outline: none;
  }
  &:hover {
    filter: brightness(0.9);
  }
  &:active {
    filter: brightness(0.7);
  }
`;


const enchance = compose(
  withState('isExample', 'setIsExample', p => p.location.hash !== '#signup'),
  withState('exampleTodo', 'setExampleTodo', true),
);

export default enchance(Landing);
