import styled from 'styled-components';

const Layout = styled.div`
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(12, [col-start] 1fr);
  grid-column-gap: 16px;
`;

export default Layout;
