import styled from 'styled-components';
import { LIGHT_COLOR } from './../../constants';

const Page = styled.div`
  font-family: 'Fira Sans';
  grid-column: col-start 1 / span 12;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  background: ${LIGHT_COLOR};
  min-height: 300px;

  @media screen and (min-width: 1000px) {
    grid-column: col-start 2 / span 10;
  }

  & > main {
    align-self: center;
    margin-bottom: 2em;
  }
`;

export default Page;
