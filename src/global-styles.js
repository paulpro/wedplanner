import { injectGlobal } from 'styled-components';
import { DARK_COLOR } from './constants';

injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Rubik:500&text=Wedlist');
@import url('https://fonts.googleapis.com/css?family=Fira+Sans:400,800&subset=cyrillic');

:root {
  font-size: 16px;
  color: ${DARK_COLOR};
}
@media screen and (min-width: 320px) {
  :root {
    font-size: calc(16px + 2 * ((100vw - 320px) / 680));
  }
}
@media screen and (min-width: 1000px) {
  :root {
    font-size: 18px;
  }
}

body {
  margin: 0;
}
`;
