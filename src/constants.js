import { css } from 'styled-components';

export const GC_USER_ID = 'graphcool-user-id';
export const GC_AUTH_TOKEN = 'graphcool-auth-token';

export const LIGHT_COLOR = css`#F8F6EA`;
export const DARK_COLOR = css`#221D1C`;
export const MUTED_COLOR = css`#ADAAA9`;
export const ACCENT_COLOR = css`#BA8D3D`;
export const ERROR_COLOR = css`#CB2B2C`;
export const SUCCESS_COLOR = css`#297045`;
