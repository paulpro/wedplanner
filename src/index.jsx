import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ApolloProvider, createNetworkInterface, ApolloClient } from 'react-apollo';

import './global-styles';
import App from './components/app';
import { GC_AUTH_TOKEN } from './constants';

// Set API link
const networkInterface = createNetworkInterface({
  uri: 'https://api.graph.cool/simple/v1/cj9abv57o1znz0163lcjs5ro7',
});

networkInterface.use([
  {
    applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {};
      }
      const token = localStorage.getItem(GC_AUTH_TOKEN);
      req.options.headers.authorization = token ? `Bearer ${token}` : null;
      next();
    },
  },
]);

// Create Apollo client
const client = new ApolloClient({
  networkInterface,
});

// Usual root component rendering
const renderApp = () =>
  render(
    <ApolloProvider client={client}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ApolloProvider>
    , document.getElementById('reactApp'));

renderApp();

// Hot Reload instructions to render
if (module.hot) {
  module.hot.accept('./components/app', () => renderApp());
}
